# MyKuber

## Description
Simple example how to run deployment, svc, autoscale and ingress controller on server where installed Kubernetes.


## Getting started
1 For ingress controller in Kubernetes I use contour, to install it use next manifest:

k apply -f https://projectcontour.io/quickstart/contour.yaml

Link to the documentation:
https://projectcontour.io/docs/1.24/

2 In order to run all services, deployments and other, download all files in folder of the server where installed Kubernetes, change domain name and run next command:

k create -f .


3 Links to the website that running in Kubernetes on the server:

1 http://resume.kuber.site/

2 http://www.kuber.site/

